package com.andor;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private final Fields fields;
    private final Legend legend;
    private int narratorPos = 0;

    private int freeShields;

    private final List<Gor> gors = new ArrayList<>();
    private final List<Skral> skrals = new ArrayList<>();
    private final List<Wardrak> wardraks = new ArrayList<>();
    private final List<Troll> trolls = new ArrayList<>();

    public Game(final Legend legend) {
	this.legend = legend;
	this.fields = new Fields();
	this.fields.readFromFile(legend.getMap());

	this.moveNarrator();
    }

    public void night() {
	// TODO Ereigniskarte
	this.moveEnemys(this.gors);
	this.moveEnemys(this.skrals);
	this.moveEnemys(this.wardraks);
	this.moveEnemys(this.trolls);
	this.moveEnemys(this.wardraks);
	// TODO brunnen
	this.moveNarrator();
    }

    private void moveEnemys(final List<? extends Enemy> enemys) {
	EnemySorter.sortByNumber(enemys);
	for (final Enemy enemy : enemys) {
	    final Field startField = enemy.getField();
	    Field next = this.fields.getField(startField.creatureWay);
	    while ((next.getEnemy() != null) && (next.number != 0)) {
		next = this.fields.getField(next.creatureWay);
	    }
	    startField.setEnemy(null);
	    if (next.number != 0) {
		next.setEnemy(enemy);
		enemy.setField(next);
	    } else {
		if (this.freeShields > 0) {
		    this.freeShields--;
		} else {
		    this.looseGame();
		}
	    }
	}
    }

    private void looseGame() {
	System.out.println("Spiel verloren");
    }

    public void moveNarrator() {
	this.narratorPos++;
	this.legend.narratorReachedField(this.narratorPos);
    }

    public void spawn(final Figure f, final int field) {
	if (f instanceof Enemy) {
	    this.spawn((Enemy) f, field);
	}
    }

    private void spawn(final Enemy e, final int field) {
	Field next = this.fields.getField(field);
	while ((next.getEnemy() != null) && (next.number != 0)) {
	    next = this.fields.getField(next.creatureWay);
	}
	if (next.number != 0) {
	    next.setEnemy(e);
	    e.setField(next);
	} else {
	    if (this.freeShields > 0) {
		this.freeShields--;
	    } else {
		this.looseGame();
	    }
	}
	if (e instanceof Gor) {
	    this.gors.add((Gor) e);
	} else if (e instanceof Skral) {
	    this.skrals.add((Skral) e);
	} else if (e instanceof Wardrak) {
	    this.wardraks.add((Wardrak) e);
	} else if (e instanceof Troll) {
	    this.trolls.add((Troll) e);
	}
    }

    public static void main(final String[] args) {
	final Game g = new Game(new LegendArrivalOfHeroes());
	for (int i = 0; i < 11; i++) {
	    g.spawn(new Gor(), 67);
	}
    }
}
