package com.andor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Fields {

    private Field[] fields;

    private boolean[][] connections;

    public Field getField(final int fieldID) {
	return this.fields[fieldID];
    }

    public void readFromFile(final String path) {
	BufferedReader reader = null;
	try {
	    reader = new BufferedReader(new FileReader(path));
	    String[] parts;
	    for (String nextLine = reader.readLine(); nextLine != null; nextLine = reader
		    .readLine()) {
		parts = nextLine.split(" +");
		if (nextLine.startsWith("F")) {
		    final int number = Integer.parseInt(parts[1]);
		    final boolean forest = parts[2].equals("t") ? true : false;
		    final boolean trader = parts[3].equals("t") ? true : false;
		    final boolean water = parts[4].equals("t") ? true : false;
		    final boolean mountain = parts[5].equals("t") ? true
			    : false;
		    final boolean fog = parts[6].equals("t") ? true : false;
		    final boolean well = parts[7].equals("t") ? true : false;
		    final int cWay = Integer.parseInt(parts[8]);
		    this.fields[number] = new Field(number, forest, trader,
			    water, mountain, fog, well, cWay);
		} else if (nextLine.startsWith("C")) {
		    final int a = Integer.parseInt(parts[1]);
		    final int b = Integer.parseInt(parts[2]);
		    this.connections[a][b] = true;
		    this.connections[b][a] = true;
		} else if (nextLine.startsWith("M")) {
		    final int num = Integer.parseInt(parts[1]);
		    this.fields = new Field[num];
		    this.connections = new boolean[num][num];
		}
	    }
	} catch (final IOException e) {
	    e.printStackTrace();
	} finally {
	    try {
		reader.close();
	    } catch (final Exception e) {
		e.printStackTrace();
	    }
	}
    }

    public static void main(final String[] args) {
	final Fields fields = new Fields();
	fields.readFromFile("res/map1.map");
	for (final boolean[] a : fields.connections) {
	    System.out.println(Arrays.toString(a));
	}
    }

}
