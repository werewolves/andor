package com.andor;

import javax.swing.JOptionPane;

public class Hero extends Figure {

    private final int health;

    private final int strength;

    private Item[] smallItems;

    private Item bigItem;

    private boolean hasHelmet;

    private int gold;

    public Hero(final int strength, final int health) {
	this.strength = strength;
	this.health = health;
    }

    public boolean pickup(final Item t) {
	switch (t.getType()) {
	case HELMET:
	    if (this.hasHelmet) {
		return false;
	    } else {
		final int answer = JOptionPane.showOptionDialog(null,
			"M�chtest du den Helm aufnehmen", "Item-Pickup",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (answer == 1) {
		    return false;
		} else {
		    this.hasHelmet = true;
		    return true;
		}
	    }
	case BIG:
	    return false;
	case FOG:
	    return false;
	case MONEY:
	    final int answer = JOptionPane.showOptionDialog(null,
		    "M�chtest du den Helm aufnehmen", "Item-Pickup",
		    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
		    null, null, null);
	    if (answer == 1) {
		return false;
	    } else {
		this.hasHelmet = true;
		return true;
	    }
	case PEASANT:
	    return false;
	case SMALL:
	    return false;
	case WELL:
	    return false;
	default:
	    return false;
	}
    }

    public static void main(final String[] args) {
	System.out.println(new Hero(1, 1).pickup(new Item(Item.TYPE.HELMET)));
    }
}
