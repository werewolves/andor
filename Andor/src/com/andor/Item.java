package com.andor;

public class Item {

    public static enum TYPE {
	SMALL, MONEY, BIG, HELMET, WELL, FOG, PEASANT
    }

    private final TYPE type;

    public TYPE getType() {
	return this.type;
    }

    private final boolean covered = true;

    public Item(final TYPE type) {
	this.type = type;
    }
}
