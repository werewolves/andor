package com.andor;

import java.util.Random;

public class Die {

    private static final int[] red = { 1, 2, 3, 4, 5, 6 };
    private static final int[] black = { 6, 6, 8, 10, 10, 12 };

    public static final Die RED_DIE = new Die(true);
    public static final Die BLACK_DIE = new Die(false);

    private final Random random;

    private final int[] myNumbers;

    private Die(final boolean isRed) {
	this.myNumbers = isRed ? Die.red : Die.black;
	this.random = new Random();
    }

    public int throwDie() {
	return this.myNumbers[this.random.nextInt(6)];
    }
}
