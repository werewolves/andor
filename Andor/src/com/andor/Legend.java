package com.andor;

public abstract class Legend {

    public abstract void narratorReachedField(final int field);

    public abstract String getMap();

}
