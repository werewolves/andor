package com.andor;

public class Field {

    public final int number;

    public final boolean hasForest;
    public final boolean hasTrader;
    public final boolean hasWater;
    public final boolean hasMountains;
    public final boolean hasFog;
    public final boolean hasWell;

    public final int creatureWay;

    private Enemy enemy;

    public Enemy getEnemy() {
	return this.enemy;
    }

    public void setEnemy(final Enemy enemy) {
	this.enemy = enemy;
    }

    public Field(final int number, final boolean forest, final boolean trader,
	    final boolean water, final boolean mountain, final boolean fog,
	    final boolean well, final int cWay) {
	this.number = number;
	this.hasForest = forest;
	this.hasTrader = trader;
	this.hasWater = water;
	this.hasMountains = mountain;
	this.hasFog = fog;
	this.hasWell = well;
	this.creatureWay = cWay;
    }
}
