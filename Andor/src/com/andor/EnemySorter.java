package com.andor;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EnemySorter {

    private EnemySorter() {
    }

    private static final Comparator<Enemy> sortByFieldNumber = new Comparator<Enemy>() {
	@Override
	public int compare(final Enemy a, final Enemy b) {
	    final int aN = a.getField().number;
	    final int bN = b.getField().number;
	    return aN > bN ? 1 : -1;
	}
    };

    public static void sortByNumber(final List<? extends Enemy> enemys) {
	Collections.sort(enemys, EnemySorter.sortByFieldNumber);
    }

}
