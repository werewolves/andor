package com.andor;

public class Enemy extends Figure {

    private final int strength;
    private final int health;

    private final int reward;

    private final boolean hasRedDice;

    public Enemy(final int strength, final int health, final int reward,
	    final boolean redDice) {
	this.strength = strength;
	this.health = health;
	this.reward = reward;
	this.hasRedDice = redDice;
    }

    public int getFightingStrength() {
	final int numOfDice = this.hasRedDice ? this.health > 6 ? 3 : 2
		: (this.health / 7) + 1;

	final int[] diceValues = new int[numOfDice];
	for (int i = 0; i < numOfDice; i++) {
	    diceValues[i] = this.hasRedDice ? Die.RED_DIE.throwDie()
		    : Die.BLACK_DIE.throwDie();
	}
	int maxVal = 0;
	int n = 0;
	for (int i = 1; i < 13; i++) {
	    for (int j = 0; j < numOfDice; j++) {
		if (diceValues[j] == i) {
		    n += i;
		}
	    }
	    if (n > maxVal) {
		maxVal = n;
	    }
	    n = 0;
	}
	return this.strength + maxVal;
    }

}
